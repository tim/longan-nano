
#include "lcd/lcd.h"
#include <string.h>

#define CONSOLE_LINE_LEN 19
#define CONSOLE_LINES 5
#define CONSOLE_BUFFER_SIZE CONSOLE_LINE_LEN * CONSOLE_LINES
#define CONSOLE_CHAR_WIDTH 8
#define CONSOLE_CHAR_HEIGHT 16
#define CONSOLE_TAB_SIZE 4

struct ConsoleBuffer {
    char buf[CONSOLE_BUFFER_SIZE];
    int  cursor;
};

void console_scroll(struct ConsoleBuffer *);
void console_write_char(struct ConsoleBuffer *, char);
void console_write_string(struct ConsoleBuffer *, char *);
void console_clear(struct ConsoleBuffer *);
void console_render(struct ConsoleBuffer *);
