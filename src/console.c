
#include "console.h"

void console_scroll(struct ConsoleBuffer * buf) {
    // move the lines up in the buffer
    memmove(&buf->buf[0], &buf->buf[CONSOLE_LINE_LEN], (CONSOLE_LINES-1)*CONSOLE_LINE_LEN);
    memset(&buf->buf[(CONSOLE_LINES-1)*CONSOLE_LINE_LEN], ' ', CONSOLE_LINE_LEN);
    buf->cursor -= CONSOLE_LINE_LEN;
    if (buf->cursor < 0) buf->cursor = 0;
}

void console_write_char(struct ConsoleBuffer * buf, char c) {
    int rem;

    // scroll the console if necessary
    while (buf->cursor >= CONSOLE_BUFFER_SIZE) console_scroll(buf);

    switch (c)
    {
    case '\n':
        // clear the rest of the line
        rem = CONSOLE_LINE_LEN - (buf->cursor % CONSOLE_LINE_LEN);
        for (int i = buf->cursor; i < buf->cursor+rem; i++) {
            buf->buf[i] = ' ';
        }

        // move the cursor to the next line
        buf->cursor += rem;
        // scroll the console if necessary
        if (buf->cursor >= CONSOLE_BUFFER_SIZE) console_scroll(buf);

        break;

    default:
        buf->buf[buf->cursor++] = c;
        break;
    }
}

void console_write_string(struct ConsoleBuffer * buf, char * str) {
    while (*str) {
        console_write_char(buf, *str++);
    }
}

void console_clear(struct ConsoleBuffer * buf) {
    // set all the characters to 0
    memset(&buf->buf[0], ' ', CONSOLE_BUFFER_SIZE);
    buf->cursor = 0;
}

void console_render(struct ConsoleBuffer * buf) {
    for (int r = 0; r < CONSOLE_LINES; r++) {
        for (int c = 0; c < CONSOLE_LINE_LEN; c++) {
            LCD_ShowChar(c*CONSOLE_CHAR_WIDTH, r*CONSOLE_CHAR_HEIGHT, buf->buf[r*CONSOLE_LINE_LEN + c], 0, LGRAY);
        }
    }
}